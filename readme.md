# readme #

GIStyleGuide's goal is to manage changes to Bootstrap's source while maintaining a styleguide dependent on this CSS framework. This will allow us to keep up with the latest Bootstrap builds while still maintaining our own style overrides. And hopefully keeping our sanity....

## Setup ##
Install the following in this order:

- [node.js](http://nodejs.org)
- [Git](http://git-scm.com/). You pretty much need this even if you have SourceTree or Github For Windows cos Bower needs git in the Windows path.
- [Grunt](http://gruntjs.com/). Install via npm. `npm install -g grunt-cli`
- [Bower](http://bower.io/). Install via npm. `npm install bower -g`

From the command line from root:

- `npm update` - updates all node's dependencies.
- `bower update` - updates all bower dependencies (Foundation, jquery, etc.).

## Creating our style guide ##
Once everything is setup (above), you can then run the following command from the root directory: `grunt lessTask`.

This grunt task currently:

- Preprocesses the LESS files.
- Concatenates all css and js files into a single file.
- Minifies all css and js files.

Look in the `\dest` folder for the output and the actual style guide. Double tap on `index.html`.