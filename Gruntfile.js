module.exports = function(grunt) {

  // Configuration goes here
  grunt.initConfig({          
        
    sass: {
        dist: {          
          files: {              
              'src/temp/all.css': 'src/sass/all.scss'
          }
        }
    },        
    
    less: {
      development: {
          options: {            
            cleancss: false
          },
          files: {
            "dest/css/gi.css": "src/less/all.less"
          }
       },
       production: {
          options: {            
            cleancss: true
          },
          files: {
            "dest/css/gi.min.css": "src/less/all.less"
          }
        }
    },        
    
    concat: {        
        // note these .js files are already minified. we are just smashing em together.
        js: {
            src: ['bower_components/jquery/jquery.min.js', 'bower_components/bootstrap/dist/js/bootstrap.min.js'],
            dest: "dest/js/gi.min.js"
        }        
    },

    cssmin: {
        minify: {
            expand: true,
            cwd: 'dest/css/',
            src: ['*.css', '!*.min.css'],
            dest: 'dest/css/',
            ext: '.min.css'
        }
    },
    
    copy: {
      customFonts: {
        files: [
          {flatten: true, expand: true, src: ['src/fonts/*'], dest: 'dest/fonts/'}
        ]
      },      
      foundationFonts: {
        files: [
          { flatten: true, 
            expand: true, 
            src: [
              'bower_components/foundation-icon-fonts/foundation-icons.eot',
              'bower_components/foundation-icon-fonts/foundation-icons.svg',
              'bower_components/foundation-icon-fonts/foundation-icons.ttf',
              'bower_components/foundation-icon-fonts/foundation-icons.woff'], 
            dest: 'dest/css/'}
        ]
      }
    },
    
    uglify: {
        options: {
            mangle: {
                except: ['jQuery']
            }  
        },
        myJsFiles: {
            files: {
                'dest/js/app.min.js': ['dest/js/app.js']
            }
        }
    }
       
  });

  // Load plugins here
  grunt.loadNpmTasks('grunt-contrib-clean');    
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-contrib-uglify'); 
  grunt.loadNpmTasks('grunt-sass');   

  // Define your tasks here
  grunt.registerTask('sassTask', ['sass', 'cssmin', 'copy:customFonts']);  
  grunt.registerTask('lessTask', ['less', 'concat', 'copy:customFonts']);  
  
};